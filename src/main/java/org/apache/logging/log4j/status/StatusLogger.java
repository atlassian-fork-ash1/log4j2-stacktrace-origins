package org.apache.logging.log4j.status;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache license, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */

import org.slf4j.LoggerFactory;

/**
 * A simple class that allows us to be log4j2 free but without changing all of the borrowed code too much that wants to
 * call StatusLogger
 */
public class StatusLogger
{
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(StatusLogger.class);

    public static class Logger
    {

        public void error(final String msg, final Object... params)
        {
            log.error(msg, params);
        }

        public void error(final Throwable t)
        {
            log.error("", t);
        }

        public void trace(final String msg, final Object... params)
        {
            log.trace(msg, params);
        }

        public void debug(final String msg, final Object... params)
        {
            log.debug(msg, params);
        }

        public void info(final String msg, final Object... params)
        {
            log.info(msg, params);
        }

        public void warn(final String msg, final Object... params)
        {
            log.warn(msg, params);
        }
    }

    private static Logger logger = new Logger();

    public static Logger getLogger()
    {
        return logger;
    }
}
